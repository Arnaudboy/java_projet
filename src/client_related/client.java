package client_related;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class client {

	public client() {
		try {
			Socket socket = new Socket("localhost", 40000);
			// mécanisme de lecture
			PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);

			BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String connectionMessage = sin.readLine();
			System.out.println("Reçu du serveur : '"+connectionMessage+"'");
			while(true) {
				Scanner command = new Scanner(System.in);
				String userChoice = command.nextLine();
				sout.println(userChoice);

				String message = sin.readLine();
				System.out.println("Reçu du serveur : '"+message+"'");
				
			}
			
		} catch(Exception exc) {
			System.out.println(exc.getMessage());
		}
	}
	public static void main(String[] args) {
		new client();

	}

}
