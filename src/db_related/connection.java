package db_related;

import java.sql.Connection;
import java.sql.DriverManager;

public class connection {
    private static Connection m_connection;
    public connection(String base, String ip, String user, String pass) {
        try {
            String url = "jdbc:mysql://"+ ip + "/"+ base ;
            m_connection = DriverManager.getConnection(url, user, pass);
            System.out.println("CONNECTÉÉÉÉÉÉÉÉ");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    
    public connection(String base) {
        this(base, "localhost", "arnaud", "arnaud22");
    }
    
    public static ResultSet requete(String req) {
    	connection con = new connection("birthday");
        try {
            Statement instruction = con.createStatement();
            ResultSet resultat = instruction.executeQuery(req);
            return resultat;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }
}
