package server_related;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

// lisprwr is for "Listener Printer Writer"
public class lisprwr {
	private String aide = "aide : aide - bye .";

	public lisprwr() {
		try {
			ServerSocket ss = new ServerSocket(40000);
			while(true) {
				System.out.println("Serveur à l'écoute...");
			
				
				Socket socket = ss.accept();
				System.out.println("connexion reçue...");
			
				PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				sout.println(aide);
				while(true) {
					String demande = sin.readLine();
					
					if(demande.equals("aide")) {
						sout.println(aide);
					} else if(demande.equals("bye")) {
						sout.println("bye OK");
						socket.close();
						break;
					}
					
				}
				
			}
			
		} catch(Exception exc) {
			System.out.println(exc.getMessage());
		}
	}
	
	public static void main(String[] args) {
		new lisprwr();
	}
}
